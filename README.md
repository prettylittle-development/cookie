# Cookie
Cookie is a small client-side javascript library that makes managing cookies easy

## API Reference

**Methods**

[cookie.set(key, value [, options])](#cookiesetkey-value-options)

[cookie.get(key)](#cookiegetkey)

#### cookie.set(key, value [, options])
Sets a cookie in the document. If the cookie does not already exist, it will be created.

**Example Usage**
```javascript
// First set a cookie and get its value
cookie.set('key', 'value');
```

#### cookie.get(key)
Returns the value of the most locally scoped cookie with the specified key.

```javascript
// First set a cookie and get its value
cookie.get('key');
```
