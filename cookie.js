
function isValidDate(date)
{
	return Object.prototype.toString.call(date) === '[object Date]' && !isNaN(date.getTime());
}

function getExpiresDate(expires, now = new Date())
{
	if (typeof expires === 'number')
	{
		expires					= expires === Infinity ? new Date('Fri, 31 Dec 9999 23:59:59 UTC') : new Date(now.getTime() + expires * 1000);
	}
	else if (typeof expires === 'string')
	{
		expires					= new Date(expires);
	}

	if (expires && !isValidDate(expires))
	{
		throw new Error('`expires` parameter cannot be converted to a valid Date instance');
	}

	return expires;
}

function getValuePairFromString(cookieString)
{
	// "=" is a valid character in a cookie value according to RFC6265, so cannot `split('=')`
	let separatorIndex			= cookieString.indexOf('=');

	// IE omits the "=" when the cookie value is an empty string
	separatorIndex				= separatorIndex < 0 ? cookieString.length : separatorIndex;

	let key						= cookieString.substr(0, separatorIndex)
	  , decodedKey;

	try
	{
		decodedKey				= decodeURIComponent(key);
	}
	catch (e)
	{
		if (console && typeof console.error === 'function')
		{
			console.error('Could not decode cookie with key "' + key + '"', e);
		}
	}

	let value					= cookieString.substr(separatorIndex + 1);
		value					= value.toLowerCase() === 'true' ? true : (value.toLowerCase() === 'false' ? false : value);

	return {
		key						: decodedKey,
		value					: value
	};
}

function getCacheFromString(documentCookie)
{
	let cache					= {}
	  , cookies					= documentCookie ? documentCookie.split('; ') : [];

	for (let i = 0; i < cookies.length; i++)
	{
		let kvp					= getValuePairFromString(cookies[i]);

		if (cache[kvp.key] === undefined)
		{
			cache[kvp.key]		= kvp.value;
		}
	}

	return cache;
}

function generateCookieString(key, value, options)
{
	key							= key.replace(/[^#$&+\^`|]/g, encodeURIComponent);
	key							= key.replace(/\(/g, '%28').replace(/\)/g, '%29');
	value						= (value + '').replace(/[^!#$&-+\--:<-\[\]-~]/g, encodeURIComponent);
	options						= options || {};

	let cookieString			= key + '=' + value;
	cookieString			   += options.path ? ';path=' + options.path : '';
	cookieString			   += options.domain ? ';domain=' + options.domain : '';
	cookieString			   += options.expires ? ';expires=' + options.expires.toUTCString() : '';
	cookieString			   += options.secure ? ';secure' : '';

	return cookieString;
}

let cookie =
{
	set : function(key, value, options)
	{
		let defaults			= {
			path				: '/',
			secure				: false
		};

		options					= Object.assign(defaults, options);
		options.expires			= getExpiresDate(value === undefined ? -1 : options.expires);
		document.cookie			= generateCookieString(key, value, options);

		return this;
	},

	get : function(key)
	{
		let cache				= getCacheFromString(document.cookie)
		  , value				= cache[key];

		return value === undefined ? undefined : decodeURIComponent(value);
	},

	expire : function(key, options)
	{
		return this.set(key, undefined, options);
	},

	enabled : function()
	{
		let testKey				= 'cookies.js'
		  , areEnabled			= this.set(testKey, '1').get(testKey) === '1';

		this.expire(testKey);

		return areEnabled;
	}
};

export { cookie };
